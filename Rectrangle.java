package com.company;

public class Rectrangle extends Shape{
    private double a;
    private double b;

    public Rectrangle() {
    }

    public Rectrangle(double a, double b) {
        this.a = a;
        this.b = b;
    }

    public Rectrangle(int area, String color, String name, double a, double b) {
        super(area, color, name);
        this.a = a;
        this.b = b;
    }

    @Override
    public double getArea() {
        return 0;
    }

    @Override
    public String toString() {
        return "Rectrangle{" +
                "a=" + a +
                ", b=" + b +
                '}';
    }
}
