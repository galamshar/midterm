package com.company;

public abstract class Shape {
    public int area;
    public String color;
    public String name;

    public Shape() {
    }

    public Shape(int area, String color, String name) {
        this.area = area;
        this.color = color;
        this.name = name;
    }

    public void setArea(int area) {
        this.area = area;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public abstract double getArea();
}
