package com.company;

public class ShapeBuilder {
    private Shape shape;
    private int area;
    private String color;
    private String name;

    public ShapeBuilder(String shapeType) {
        switch (shapeType.toLowerCase()){
            case("circle"):
                shape = new Circle();
                break;
            case("rectangle"):
                shape = new Rectrangle();
                break;
            case("triangle"):
                shape = new Triangle();
                break;
            default:
                break;
        }

    }

    public ShapeBuilder selectColor(String color){
        shape.color = color;
        return this;
    }

    public ShapeBuilder selectName(String name){
        shape.name = name;
        return this;
    }

    public ShapeBuilder selectArea(int area){
        shape.area = area;
        return this;
    }

    public Shape build(){return shape;}
}
